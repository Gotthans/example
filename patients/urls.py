from django.urls import path
from .views import patients_create_view, patients_list_view, patients_delete_view, patients_update_view


app_name = "patients"
urlpatterns = [
    path('', patients_list_view),
    path('create/', patients_create_view),
    path('<int:id>/edit/', patients_update_view),
    path('<int:id>/delete/', patients_delete_view)
]