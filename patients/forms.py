from django import forms
from .models import Patients
from insurance.models import InsuranceCompany


class PatientsForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PatientsForm, self).__init__(*args, **kwargs)
        select_insurance_data = InsuranceCompany.objects.all()
        list_of_choices = []
        for i in range(0, select_insurance_data.count()):
            list_of_choices.append((select_insurance_data[i].pk, select_insurance_data[i].number_of_insurance_company+', '+select_insurance_data[i].name_of_insurance_company))

        print(list_of_choices)
        self.fields["insurance_company"].choices = list_of_choices

    first_name = forms.CharField(required = True, max_length = 10)
    surname = forms.CharField(required = True, max_length = 10)
    age = forms.CharField(max_length=3)
    insurance_company = forms.ModelChoiceField(required = True, queryset = InsuranceCompany.objects.all())

    class Meta:
        model = Patients
        fields = [
            'first_name',
            'surname',
            'age',
            'insurance_company',
        ]    
