from django.shortcuts import render
from .models import Patients
from .forms import PatientsForm
from django.shortcuts import render, redirect, get_object_or_404


def patients_create_view(request):
    form = PatientsForm(request.POST or None)

    if form.is_valid():
        form.save()
        form = PatientsForm()
        return redirect('../')
    context = {
        'form': form
    }
    return render(request, "patients/create.html", context)


def patients_list_view(request):
    data = Patients.objects.all()
    context = {
        'data': data
    }
    return render(request, "patients/list_view.html", context)

def patients_delete_view(request, id):
    obj = get_object_or_404(Patients, id=id)
    if request.method == "POST" and request.POST.get("Yes") == "Yes":
        obj.delete()
        return redirect('../../')
    elif request.POST.get("No") == "No":
        return redirect('../../')
    context = {
        "object": obj
    }
    return render(request, "patients/delete.html", context)


def patients_update_view(request, id=id):
    form = PatientsForm()
    obj = get_object_or_404(Patients, id=id)
    form = PatientsForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
        return redirect('../../')
    context = {
        'form': form
    }
    return render(request, "patients/create.html", context)