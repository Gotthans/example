from django.db import models
from insurance.models import InsuranceCompany
# Create your models here.


class Patients(models.Model):
    first_name  = models.CharField(max_length = 120)
    surname     = models.CharField(max_length = 120)
    age         = models.CharField(blank = True, null = True, max_length = 3)
    insurance_company   = models.ForeignKey(InsuranceCompany, on_delete = models.CASCADE)

    class Meta:
        app_label = 'patients'

    def __str__(self):
        return "%s, %s, %s" % (self.pk, self.first_name, self.surname)