from django.urls import path
from .views import (
    authentificate_view,
    logout_view,
    home_view,
    
)

app_name = 'login'
urlpatterns = [
    path('auth', authentificate_view),
    path('', home_view),
    path('logout_view', logout_view),
]
