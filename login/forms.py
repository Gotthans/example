from django import forms


class LoginForm(forms.Form):
    username          = forms.CharField(required = True)
    password          = forms.CharField(required = True) # max_length = required
    
    class Meta:

        fields = [
            'username',
            'password',
            
        ]