from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, authenticate, login
from django.shortcuts import render, redirect
from .forms import LoginForm

# Create your views here.
def logout_view(request):
    logout(request)
    return redirect('../../')

def home_view(request):
    form = LoginForm()
    context = {
        'object_list': form,
    }
    return render(request,'login.html', context)

def authentificate_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        return redirect('../patients')
        
    else:
        # Return an 'invalid login' error message.
        return redirect('../')