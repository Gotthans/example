from django.db import models

# Create your models here.

class InsuranceCompany(models.Model):
    name_of_insurance_company   = models.CharField(max_length=200)
    number_of_insurance_company = models.CharField(max_length=4)

    class Meta:
        app_label = 'insurance'

    def __str__(self):
        return ('%s') % (self.pk)