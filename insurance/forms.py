from django import forms
from .models import InsuranceCompany


class InsuranceCompanyForm(forms.ModelForm):
    name_of_insurance_company       = forms.CharField(required = True, max_length = 200, label = "Name of Insurance Company")
    number_of_insurance_company     = forms.CharField(required = True, max_length = 4, label = "Number of Insurance Company")

    class Meta:
        model = InsuranceCompany
        fields = [
            'name_of_insurance_company',
            'number_of_insurance_company',
        ]