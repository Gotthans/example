function checkDate(m, d, y) {
    return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
}

function birthNumber(viewValue) {
    
                var regex = /^\s*(\d\d)(\d\d)(\d\d)[ /]*(\d\d\d)(\d?)\s*$/;
                var match = regex.exec(viewValue);
                
                if (match) {
                    var year = parseInt(match[1]);
                    var month = parseInt(match[2]);
                    var day = parseInt(match[3]);
                    var ext = parseInt(match[4]);
                    var c = match[5];
                    
                    var mod;
                    var left;
                    var right;

                    left  = parseInt(match[1][0])+parseInt(match[2][0])+parseInt(match[3][0])+parseInt(match[4][0])+parseInt(match[4][2]);
                    if (parseInt(match[5][0])){
                        right = parseInt(match[1][1])+parseInt(match[2][1])+parseInt(match[3][1])+parseInt(match[4][1])+parseInt(match[5][0]);
                    }
                        else {
                        right = parseInt(match[1][1])+parseInt(match[2][1])+parseInt(match[3][1])+parseInt(match[4][1]);
                        }
                    
                    if (((left-right)%11)){
                        return false;
                    } 

                    if (c === '') { //rodna cisla nakonci se 3mi cislicemi
                        year += year < 54 ? 1900 : 1800;
                        
                    } else {

                        year += year < 54 ? 2000 : 1900;
                        
                    }

                    if (month > 70 && year > 2003) {
                        month -= 70;
                    } else if (month > 50) {
                        month -= 50;
                    } else if (month > 20 && year > 2003) {
                        month -= 20;
                    }
                    
                    return [month, day, year];
                }

                // it is invalid
                return false;
            };


//----------------------------------------------------------
//  Po dopsani pocka 2 sekundy, pote provede parsovani
//----------------------------------------------------------
//setup before functions
let typingTimer;                //timer identifier
let doneTypingInterval = 2000;  //time in ms (5 seconds)
let myInput = document.getElementById('id_rodne_cislo');

//on keyup, start the countdown
myInput.addEventListener('keyup', () => {
    clearTimeout(typingTimer);
    if (myInput.value) {
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }
});

//user is "finished typing," do something
function doneTyping () {
    var datum = birthNumber(myInput.value); // format: mesic, den, rok
    //dela se double check prvni funkce ma zakladni kontrolu, druha kontroluje spravnost data
    if (checkDate(datum[0], datum[1], datum[2])){
        console.log(datum);
        console.log(datum[0]);
        console.log(datum[1]);
        console.log(datum[2]);

        if (datum[0]<10){
            var datum1 = ("0" + datum[0]).slice(-2);
            //console.log(datum1);
        }
        else{
            var datum1 = datum[0];
        }

        if (datum[1]<10){
            var datum2 = ("0" + datum[1]).slice(-2);
            //console.log(datum2);
        }
        else{
            var datum2 = datum[1];
        }
        
        //$("#id_datum_narozeni").val(datum1+'.'+datum2+'.'+datum[2]);
        $("#id_datum_narozeni").val(datum[2]+'-'+datum1+'-'+datum2);
    }
    else{
        console.log(datum);
        $('#id_datum_narozeni').val("Nespravne RC");
    }
    //do something
}